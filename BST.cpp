#include<iostream>
using namespace std;

struct node
{
	char data;
	node *left, *right;
};

struct node_list
{
  node *data;
  node_list *next;
};

void input(node *&root, char d)
{
	root=new node;
	root->data=d;
	root->left=0;
	root->right=0;
	return;
}

void preorder(node *root){
  if(root == NULL) return;
  cout<<root->data;
  preorder(root->left);
  preorder(root->right);
}

void inorder(node *root){
  if(root == NULL) return;
  inorder(root->left);
  cout<<root->data;
  inorder(root->right);
}

void postorder(node *root){
  if(root == NULL) return;
  postorder(root->left);
  postorder(root->right);
  cout<<root->data;  
}

void dodajUCekanje(node_list *& lista, node * za_ubaciti){
  node_list * novi = new node_list;
  novi->data = za_ubaciti;
  novi->next = 0;
  
  if(lista==0){
    lista = novi;
  }else{
    node_list * trenutni = lista;
    while(trenutni->next != 0){
      trenutni = trenutni->next;
      }    
    trenutni->next = novi;
  }
}

node * brisiIzListe(node_list *& lista){
  if(lista){
        //cout<<lista->data->data;
        node_list * zaBrisati = lista;
        lista = lista->next;
  
        node *vrati=zaBrisati->data;
        delete zaBrisati;
        return vrati;
        
  }else{
    return 0;
  }
}

void najprijeUSirinu(node *root){
  node * trenutni = root;
  node_list * lista = 0;
  while(trenutni){
    cout<<trenutni->data;
    if(trenutni->left)  dodajUCekanje(lista, trenutni->left);
    if(trenutni->right) dodajUCekanje(lista, trenutni->right);
    trenutni = brisiIzListe(lista);
  }
}

int main()
{
	node *root=0;
	input(root, 'A');
	// unos 1. razine
	input(root->left, 'B');
	input(root->right, 'C');
	// unos 2. razine
	input(root->left->left, 'D');
	input(root->left->right, 'E');
	input(root->right->left, 'F');
	input(root->right->right, 'G');
	
        
        //preorder(root);
        /*cout<<endl;
        inorder(root);
        cout<<endl;
        postorder(root); // ovime ide dealokacija
        cout<<endl;
        */
        
        najprijeUSirinu(root);
        cout<<endl;
        
        return 0;
}